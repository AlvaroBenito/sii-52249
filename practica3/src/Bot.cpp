#include "Bot.h"
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

Bot::Bot(){
	memo=NULL;
}
Bot::~Bot(){}
void Bot::openFichero(){
	fd=open("Memoria", O_RDWR);	
}

void Bot::proyectar(){
	void *p;
	struct stat bstat;
	fstat(fd,&bstat);
	p=mmap(NULL,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED, fd, 0);
	memo=(DatosMemCompartida *)p;	
	close(fd);
	
}
void Bot::actuador(){
	if(memo->esfera.centro.y<(memo->raqueta1.y2+memo->raqueta1.y1)/2)
		memo->accion=-1;
	else if(memo->esfera.centro.y>(memo->raqueta1.y2+memo->raqueta1.y1)/2)
		memo->accion=1;
	else memo->accion=0;
}
